package com.touriexpert.ws.exception;

public class ConnectionException extends Exception {

	private static final long serialVersionUID = -8124945214891848056L;

	public ConnectionException(Exception e){
		super(e);
	}

	public ConnectionException(String message){
		super(message);
	}
}
