package com.touriexpert.ws.exception;

public class ParameterException extends Exception {
	
	private static final long serialVersionUID = -391767110133475413L;

	public ParameterException(Exception e){
		super(e);
	}
}
