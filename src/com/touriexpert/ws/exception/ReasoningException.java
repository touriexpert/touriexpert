package com.touriexpert.ws.exception;

public class ReasoningException extends Exception {

	private static final long serialVersionUID = 236631678143194258L;

	public ReasoningException(Exception e){
		super(e);
	}

	public ReasoningException(String message){
		super(message);
	}
}
