package com.touriexpert.ws.exception;

public class FactParserException extends Exception {

	private static final long serialVersionUID = 226417723592586707L;

	public FactParserException (String message) {
		super(message);
	}
	
}
