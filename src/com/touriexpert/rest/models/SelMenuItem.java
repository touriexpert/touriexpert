package com.touriexpert.rest.models;

public class SelMenuItem {
	private int id;
    private Double prezzo;
    private String nome;
    private String descrizione;

    public SelMenuItem(){
        this.prezzo = 0.0;
        this.nome = "";
        this.descrizione = "";
    }

    public Double getPrezzo() {
        return prezzo;
    }

    public void setPrezzo(Double prezzo) {
        this.prezzo = prezzo;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getDescrizione() {
        return descrizione;
    }

    public void setDescrizione(String descrizione) {
        this.descrizione = descrizione;
    }
    
    public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return "Piatto [id=" + id + ", prezzo=" + prezzo + ", nome=" + nome + ", descrizione=" + descrizione + "]";
	}
}
