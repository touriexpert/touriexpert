package com.touriexpert.rest.models;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class ExtPrenotazione {
    private int peopleMenu;
    private int peopleNoMenu;
	private String date;
    private Date realDate;
    private String time;
    private Timestamp realTime;

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
    
    public void setRealTime(){
    	DateFormat format = new SimpleDateFormat("HH:mm", Locale.ITALY);
    	Date realTimeDate = null;
		try {
			realTimeDate = format.parse(time);
		} catch (ParseException e) {
			this.realTime = null;
		}
    	Timestamp tempTimeStamp = new Timestamp(realTimeDate.getTime());
    	this.realTime = tempTimeStamp;
    }
    
    public void setRealDate() {
        //TODO Locale troppo specifico. Meglio settarlo tramite configurazione
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.ITALY);
        try {
			this.realDate = format.parse(this.date);
		} catch (ParseException e) {
			this.realDate = null;
		}
    }

    public void setDate(String date) {
        this.date = date;

    }

    public String getDate() {
        return this.date;
    }
    
    public Date getRealDate() {
        return this.realDate;
    }
    
    public Timestamp getRealTime() {
        return this.realTime;
    }
    
    public int getPeopleMenu() {
		return peopleMenu;
	}

	public void setPeopleMenu(int peopleMenu) {
		this.peopleMenu = peopleMenu;
	}

	public int getPeopleNoMenu() {
		return peopleNoMenu;
	}

	public void setPeopleNoMenu(int peopleNoMenu) {
		this.peopleNoMenu = peopleNoMenu;
	}
}
