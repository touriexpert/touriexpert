package com.touriexpert.rest.models;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import com.touriexpert.rest.reasoning.fact.Fact;
import com.touriexpert.rest.reasoning.fact.RankFact;

import rdb.database.pojo.Articolo;
import rdb.database.pojo.Categoria;
import rdb.database.pojo.Ricetta;

public class ReasArticolo  {
	@SuppressWarnings("unused")
	private static final long serialVersionUID = -1534112858284134032L;
	private int id;
	private Articolo articolo;
	private Fact[] facts;
	private Double ranking;
	private Map<String, Integer> schedaMercadini;
	
	public ReasArticolo(Articolo articolo){
		this.articolo = articolo;
		this.id = articolo.getIdarticolo();
		this.ranking = 0.0;
		this.schedaMercadini = new HashMap<String, Integer>();
	}
	
	public Integer getId(){
		return this.id;
	}
	
	public void setArticolo(Articolo articolo) {
		this.articolo = articolo;
	}

	public String getNome(){
		return articolo.getNome();
	}
	
	public String getDescrizione(){
		return articolo.getDescrizione();
	}
	
	public Double getPrezzo(){
		return articolo.getPrezzo();
	}

	public Fact[] getFacts() {
		return facts;
	}

	public void setFacts(Fact[] facts) {
		this.facts = facts;
	}

	public Set<Ricetta> getRicette(){
		return articolo.getRicette();
	}

	public Set<Categoria> getCategorie(){
		return articolo.getCategorie();
	}

	public Double getRanking() {
		return ranking;
	}

	public void setRanking(Double ranking) {
		this.ranking = ranking;
	}
	
	public void updateRank(Double value) {
		this.ranking += value;
		for(Fact fact : this.facts) {
			if (fact instanceof RankFact) {
				((RankFact) fact).setRanking(value);
			}
		}
	}
	
	public Integer getMercadiniValue(String sensazione) {
		return this.schedaMercadini.get(sensazione);
	}
	
	public void setSchedaMercadini(Map<String, Integer> mercadiniValues) {
		this.schedaMercadini = mercadiniValues;
	}
	
	public Map<String, Integer> getSchedaMercadini() {
		return this.schedaMercadini;
	}
}
