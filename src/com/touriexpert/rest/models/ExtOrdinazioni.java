package com.touriexpert.rest.models;

import java.util.List;

public class ExtOrdinazioni {

    private List<SelMenuItem> menu;

	private String owner;
    
    private String price;

	public List<SelMenuItem> getMenu() {
		return menu;
	}

	public void setMenu(List<SelMenuItem> menu) {
		this.menu = menu;
	}

	public String getOwner() {
		return owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}
	
    @Override
	public String toString() {
		return "MenuScelti [menu=" + menu + ", owner=" + owner + ", price=" + price + "]";
	}

}
