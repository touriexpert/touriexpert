package com.touriexpert.rest.reasoning.fact;

public class CostoPortataFact extends Fact {
	private int idArticolo;
	private double costo;
	
	public CostoPortataFact(){
		super();
	}
	
	public CostoPortataFact(int id){
		super();
		this.id = id;
	}
	
	public CostoPortataFact(int id, double costo){
		super();
		this.id = id;
		this.certezza = 1.0;
		this.costo = costo;
	}

	public int getIdArticolo() {
		return idArticolo;
	}

	public void setIdArticolo(int idArticolo) {
		this.idArticolo = idArticolo;
	}

	public double getCosto() {
		return costo;
	}

	public void setCosto(double costo) {
		this.costo = costo;
	}
	
	@Override
	public String getBody(){
		return "costo(" + this.idArticolo + Fact.BODYSEPARATOR + this.costo + ")";
	}
	
	@Override
	public void setBody(String factBody) {
		String bodyContent = factBody.substring(factBody.indexOf("(") + 1, factBody.indexOf(")"));
		String[] bodyData = bodyContent.split(",");
		this.idArticolo = Integer.parseInt(bodyData[0]);
		this.costo = Double.parseDouble(bodyData[1]);
	}
}