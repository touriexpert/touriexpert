package com.touriexpert.rest.reasoning.fact;

public class RankFact extends Fact {
	private int idArticolo;
	private Double ranking;
	
	public RankFact(){
		super();
	}
	
	public RankFact(int id){
		super();
		this.id = id;
		this.ranking = 0.0;
	}

	public Double getRanking() {
		return ranking;
	}

	public void setRanking(Double ranking) {
		this.ranking = ranking;
	}

	public int getIdArticolo() {
		return idArticolo;
	}

	public void setIdArticolo(int idArticolo) {
		this.idArticolo = idArticolo;
	}

	@Override
	public String getBody() {
		return "rank(" + this.idArticolo + Fact.BODYSEPARATOR + this.ranking + ")";
	}
	
	@Override
	public void setBody(String factBody) {
		String bodyContent = factBody.substring(factBody.indexOf("(") + 1, factBody.indexOf(")"));
		String[] bodyData = bodyContent.split(",");
		this.idArticolo = Integer.parseInt(bodyData[0]);
		this.ranking = Double.parseDouble(bodyData[1]);
	}
}
