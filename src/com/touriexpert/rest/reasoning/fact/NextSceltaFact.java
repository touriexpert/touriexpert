package com.touriexpert.rest.reasoning.fact;

public class NextSceltaFact extends Fact {
	private int userid;
	private String nextScelta;
	
	public NextSceltaFact() {
		super();
	}

	public int getUserid() {
		return userid;
	}

	public void setUserid(int userid) {
		this.userid = userid;
	}

	public String getNextScelta() {
		return nextScelta;
	}

	public void setNextScelta(String nextScelta) {
		this.nextScelta = nextScelta;
	}

	@Override
	public void setBody(String factBody) {
		String bodyContent = factBody.substring(factBody.indexOf("(") + 1, factBody.indexOf(")"));
		String[] bodyData = bodyContent.split(",");
		this.userid = Integer.parseInt(bodyData[0]);
		this.nextScelta = bodyData[1];
	}

	@Override
	public String getBody() {
		return "next_scelta(" + this.userid + Fact.BODYSEPARATOR + this.nextScelta + ")";
	}

}
