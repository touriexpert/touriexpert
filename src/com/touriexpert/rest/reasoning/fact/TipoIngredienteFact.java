package com.touriexpert.rest.reasoning.fact;

public class TipoIngredienteFact extends Fact {
	private String tipo;
	private String ingrediente;

	public TipoIngredienteFact(int id) {
		super();
		this.id = id;
	}
	
	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getIngrediente() {
		return ingrediente;
	}

	public void setIngrediente(String ingrediente) {
		this.ingrediente = ingrediente;
	}

	@Override
	public String getBody() {
		return "tipo_ingrediente(" + this.ingrediente + Fact.BODYSEPARATOR +  this.tipo + ")";
	}

	@Override
	public void setBody(String factBody) {
		String bodyContent = factBody.substring(factBody.indexOf("(") + 1, factBody.indexOf(")"));
		String[] bodyData = bodyContent.split(",");
		this.ingrediente = bodyData[0];
		this.tipo = bodyData[1];
	}
}
