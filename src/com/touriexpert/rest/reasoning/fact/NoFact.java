package com.touriexpert.rest.reasoning.fact;

public class NoFact extends Fact {
	private String innerBody;
	
	public NoFact(){
		super();
	}
	
	public NoFact(int id, String innerBody){
		super();
		this.id = id;
		this.innerBody = innerBody;
	}
	@Override
	public void setBody(String factBody) {
		int factBodyLength = factBody.length();
		String bodyContent = null;
		if (factBody.charAt(factBodyLength - 1) == '.') {
			bodyContent = factBody.substring(factBody.indexOf("(") + 1, factBody.indexOf(")."));
		} else {
			bodyContent = factBody.substring(factBody.indexOf("(") + 1, factBody.indexOf(")"));
		}
		this.innerBody = bodyContent;
	}

	@Override
	public String getBody() {
		return "not(" + this.innerBody + ")";
	}

}
