package com.touriexpert.rest.reasoning.fact;

public class AbbinaFact  extends Fact {

	private Integer idFirstArticolo;
	private Integer idSecondArticolo;
	private String tipoAbbinamento;
	
	public Integer getIdSecondArticolo() {
		return idSecondArticolo;
	}

	public void setIdSecondArticolo(Integer idSecondArticolo) {
		this.idSecondArticolo = idSecondArticolo;
	}

	public Integer getIdFirstArticolo() {
		return idFirstArticolo;
	}

	public void setIdFirstArticolo(Integer idFirstArticolo) {
		this.idFirstArticolo = idFirstArticolo;
	}

	@Override
	public void setBody(String factBody) {
		String bodyContent = factBody.substring(factBody.indexOf("(") + 1, factBody.indexOf(")"));
		String[] bodyData = bodyContent.split(",");
		this.tipoAbbinamento = factBody.substring(0, factBody.indexOf("("));
		this.idFirstArticolo = Integer.parseInt(bodyData[0]);
		this.idSecondArticolo = Integer.parseInt(bodyData[1]);
	}

	@Override
	public String getBody() {
		return this.tipoAbbinamento + "(" + this.idFirstArticolo + Fact.BODYSEPARATOR + this.idSecondArticolo + ")";
	}

}
