package com.touriexpert.rest.reasoning.fact;

import com.touriexpert.ws.exception.FactParserException;

public class FactParser {
	public Fact getFact(String factBody) throws FactParserException {
		Fact fact = null;
		if(factBody.contains("update_rank")){
			UpdateRankFact rankFact = new UpdateRankFact();
			rankFact.setBody(factBody);
			fact = rankFact;
		} else if(factBody.contains("rank")){
			RankFact rankFact = new RankFact();
			rankFact.setBody(factBody);
			fact = rankFact;
		} else if(factBody.contains("tipo_portata")){
			TipoPortataFact tipoPortataFact = new TipoPortataFact();
			tipoPortataFact.setBody(factBody);
			fact = tipoPortataFact;
		} else if(factBody.contains("costo")){
			CostoPortataFact costoFact = new CostoPortataFact();
			costoFact.setBody(factBody);
			fact = costoFact;
		} else if(factBody.contains("portata_contiene")){
			ContieneIngredienteFact tipoPortataFact = new ContieneIngredienteFact();
			tipoPortataFact.setBody(factBody);
			fact = tipoPortataFact;
		} else if(factBody.contains("cat_scelta")){
			CategoriaSceltaFact categoriasSceltaFact = new CategoriaSceltaFact();
			categoriasSceltaFact.setBody(factBody);
			fact = categoriasSceltaFact;
		} else if(factBody.contains("next_scelta")){
			NextSceltaFact nextSceltaFact = new NextSceltaFact();
			nextSceltaFact.setBody(factBody);
			fact = nextSceltaFact;
		} else if(factBody.contains("cat_portata")){
			CategoriaPortataFact factCategoria = new CategoriaPortataFact();
			factCategoria.setBody(factBody);
			fact = factCategoria;
		} else if(factBody.contains("vietata")){
			VietataFact vietataFact = new VietataFact();
			vietataFact.setBody(factBody);
			fact = vietataFact;
		} else if(factBody.contains("not")){
			NoFact notFact = new NoFact();
			notFact.setBody(factBody);
			fact = notFact;
		} else if(factBody.contains("liv_sapore")){
			LivelloSaporeFact livelloSaporeFact = new LivelloSaporeFact();
			livelloSaporeFact.setBody(factBody);
			fact = livelloSaporeFact;
		} else if(factBody.contains("abbina")){
			AbbinaFact abbinaFact = new AbbinaFact();
			abbinaFact.setBody(factBody);
			fact = abbinaFact;
		} else {
			throw new FactParserException(new String("FactBody: " + factBody + "non riconosciuto!"));
		}
		return fact;
	}
}
