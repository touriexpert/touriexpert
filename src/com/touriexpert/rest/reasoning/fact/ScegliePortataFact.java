package com.touriexpert.rest.reasoning.fact;

public class ScegliePortataFact extends Fact {
	private int idArticolo;
	private int idUtente;

	public int getIdArticolo() {
		return idArticolo;
	}

	public void setIdArticolo(int idArticolo) {
		this.idArticolo = idArticolo;
	}

	public int getIdUtente() {
		return idUtente;
	}

	public void setIdUtente(int idUtente) {
		this.idUtente = idUtente;
	}

	@Override
	public String getBody() {
		return "sceglie_portata(" + this.idUtente + Fact.BODYSEPARATOR + this.idArticolo + ")";
	}
	
	@Override
	public void setBody(String factBody) {
		String bodyContent = factBody.substring(factBody.indexOf("(") + 1, factBody.indexOf(")"));
		String[] bodyData = bodyContent.split(",");
		this.idUtente = Integer.parseInt(bodyData[0]);
		this.idArticolo = Integer.parseInt(bodyData[1]);
	}
}