package com.touriexpert.rest.reasoning.fact;

public class LivelloSaporeFact extends Fact {
	private int idArticolo;
	private int livelloSapore;

	public int getIdArticolo() {
		return idArticolo;
	}

	public void setIdArticolo(int idArticolo) {
		this.idArticolo = idArticolo;
	}

	public int getLivelloSapore() {
		return livelloSapore;
	}

	public void setLivelloSapore(int livelloSapore) {
		this.livelloSapore = livelloSapore;
	}

	@Override
	public void setBody(String factBody) {
		String bodyContent = factBody.substring(factBody.indexOf("(") + 1, factBody.indexOf(")"));
		String[] bodyData = bodyContent.split(",");
		this.idArticolo = Integer.parseInt(bodyData[0]);
		this.livelloSapore = Integer.parseInt(bodyData[1]);
	}

	@Override
	public String getBody() {
		return "liv_sapore(" + this.idArticolo + Fact.BODYSEPARATOR + this.livelloSapore + ")";
	}

}
