package com.touriexpert.rest.reasoning.fact;

public class CategoriaPortataFact extends Fact {
	private int idArticolo;
	private String categoria;
	
	public CategoriaPortataFact() {
		super();
	}
	
	public CategoriaPortataFact(int id) {
		this.id = id;
	}

	public int getIdArticolo() {
		return idArticolo;
	}

	public void setIdArticolo(int idArticolo) {
		this.idArticolo = idArticolo;
	}

	public String getCategoria() {
		return categoria;
	}

	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}

	@Override
	public String getBody() {
		return "cat_portata(" + this.idArticolo + Fact.BODYSEPARATOR + this.categoria + ")";
	}
	
	@Override
	public void setBody(String factBody) {
		String bodyContent = factBody.substring(factBody.indexOf("(") + 1, factBody.indexOf(")"));
		String[] bodyData = bodyContent.split(",");
		this.idArticolo = Integer.parseInt(bodyData[0]);
		this.categoria = bodyData[1];
	}
}