package com.touriexpert.rest.reasoning.fact;

public class UpdateRankFact extends Fact {
	private int idArticolo;
	private double value;

	public int getIdArticolo() {
		return idArticolo;
	}

	public void setIdArticolo(int idArticolo) {
		this.idArticolo = idArticolo;
	}

	public double getValue() {
		return value;
	}

	public void setValue(double value) {
		this.value = value;
	}

	@Override
	public void setBody(String factBody) {
		String bodyContent = factBody.substring(factBody.indexOf("(") + 1, factBody.indexOf(")"));
		String[] bodyData = bodyContent.split(",");
		this.idArticolo = Integer.parseInt(bodyData[0]);
		this.value = Double.parseDouble(bodyData[1]);
	}

	@Override
	public String getBody() {
		return "update_rank(" + this.idArticolo + Fact.BODYSEPARATOR + this.value + ")";
	}

}
