package com.touriexpert.rest.reasoning.fact;

public class CategoriaSceltaFact extends Fact {
	private int userId;
	private String categoria;
	
	public CategoriaSceltaFact() {
		super();
	}
	
	public int getUserId() {
		return userId;
	}
	
	public void setUserId(int userId) {
		this.userId = userId;
	}
	
	public String getCategoria() {
		return categoria;
	}
	
	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}
	
	@Override
	public void setBody(String factBody) {
		String bodyContent = factBody.substring(factBody.indexOf("(") + 1, factBody.indexOf(")"));
		String[] bodyData = bodyContent.split(",");
		this.userId = Integer.parseInt(bodyData[0]);
		this.categoria = bodyData[1];
	}
	@Override
	public String getBody() {
		return "cat_scelta(" + this.userId + Fact.BODYSEPARATOR + this.categoria + ")";
	}
	
}
