package com.touriexpert.rest.reasoning.fact;

public abstract class Fact {
	protected int id;
	protected double certezza;
	protected static final String BODYSEPARATOR = ","; 
	
	Fact(){
		this.certezza = 1.0;
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public double getCertezza() {
		return certezza;
	}

	public void setCertezza(double certezza) {
		this.certezza = certezza;
	}

	@Override
	public String toString() {
		return "fact(" + this.id + ", " + this.getBody() + "," + this.certezza + ").";
	}
	abstract public void setBody(String factBody);
	abstract public String getBody();
}