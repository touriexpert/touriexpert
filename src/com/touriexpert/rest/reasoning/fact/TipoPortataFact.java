package com.touriexpert.rest.reasoning.fact;

public class TipoPortataFact extends Fact {
	private String tipo;
	private Integer idArticolo;
	
	public TipoPortataFact(){
		super();
	}
	
	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public Integer getIdArticolo() {
		return idArticolo;
	}

	public void setIdArticolo(Integer idArticolo) {
		this.idArticolo = idArticolo;
	}

	@Override
	public String getBody() {
		return "tipo_portata(" + this.idArticolo + "," + this.tipo + ")";
	}

	@Override
	public void setBody(String factBody) {
		String bodyContent = factBody.substring(factBody.indexOf("(") + 1, factBody.indexOf(")"));
		String[] bodyData = bodyContent.split(Fact.BODYSEPARATOR);
		this.idArticolo = Integer.parseInt(bodyData[0]);
		this.tipo = bodyData[1];
	}

}
