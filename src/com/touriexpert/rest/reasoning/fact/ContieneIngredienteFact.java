package com.touriexpert.rest.reasoning.fact;

public class ContieneIngredienteFact extends Fact {
	private int idArticolo;
	private String ingrediente;
	
	public ContieneIngredienteFact(){
		super();
	}
	
	public ContieneIngredienteFact(int id){
		super();
		this.id = id;
	}

	public int getIdArticolo() {
		return idArticolo;
	}

	public void setIdArticolo(int idArticolo) {
		this.idArticolo = idArticolo;
	}

	public String getIngrediente() {
		return ingrediente;
	}

	public void setIngrediente(String ingrediente) {
		this.ingrediente = ingrediente;
	}
	
	@Override
	public String getBody(){
		return "portata_contiene(" + this.idArticolo + Fact.BODYSEPARATOR + this.ingrediente + ")";
	}

	@Override
	public void setBody(String factBody) {
		String bodyContent = factBody.substring(factBody.indexOf("(") + 1, factBody.indexOf(")"));
		String[] bodyData = bodyContent.split(",");
		this.idArticolo = Integer.parseInt(bodyData[0]);
		this.ingrediente = bodyData[1];
	}
}