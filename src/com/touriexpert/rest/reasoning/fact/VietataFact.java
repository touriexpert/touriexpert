package com.touriexpert.rest.reasoning.fact;

public class VietataFact extends Fact {
	private int idArticolo;
	
	public VietataFact() {
		super();
	}
	
	public VietataFact(int id){
		super();
		this.id = id;
	}
	
	public int getIdArticolo() {
		return idArticolo;
	}

	public void setIdArticolo(int idArticolo) {
		this.idArticolo = idArticolo;
	}

	@Override
	public void setBody(String factBody) {
		String bodyContent = factBody.substring(factBody.indexOf("(") + 1, factBody.indexOf(")"));
		this.idArticolo = Integer.parseInt(bodyContent);
	}

	@Override
	public String getBody() {
		return "vietata(" + this.idArticolo + ")";
	}

}
