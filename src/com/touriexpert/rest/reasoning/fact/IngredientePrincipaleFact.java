package com.touriexpert.rest.reasoning.fact;

public class IngredientePrincipaleFact extends Fact {
	private Integer idArticolo;
	private String ingredientePrincipale;
	
	public IngredientePrincipaleFact(int id){
		this.id = id;
	}
	
	public Integer getIdArticolo() {
		return idArticolo;
	}
	
	public void setIdArticolo(Integer idArticolo) {
		this.idArticolo = idArticolo;
	}
	
	public String getIngredientePrincipale() {
		return ingredientePrincipale;
	}
	
	public void setIngredientePrincipale(String ingredientePrincipale) {
		this.ingredientePrincipale = ingredientePrincipale;
	}

	@Override
	public String getBody() {
		return "ingrediente_pr(" + this.idArticolo + Fact.BODYSEPARATOR + this.ingredientePrincipale + ")";
	}

	@Override
	public void setBody(String factBody) {
		// TODO Auto-generated method stub
		
	}
	
}
