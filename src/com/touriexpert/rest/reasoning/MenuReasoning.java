package com.touriexpert.rest.reasoning;

import java.net.URL;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang3.ArrayUtils;

import com.google.gson.GsonBuilder;
import com.touriexpert.rest.models.ReasArticolo;
import com.touriexpert.rest.reasoning.fact.CategoriaPortataFact;
import com.touriexpert.rest.reasoning.fact.ContieneIngredienteFact;
import com.touriexpert.rest.reasoning.fact.CostoPortataFact;
import com.touriexpert.rest.reasoning.fact.Fact;
import com.touriexpert.rest.reasoning.fact.IngredientePrincipaleFact;
import com.touriexpert.rest.reasoning.fact.RankFact;
import com.touriexpert.rest.reasoning.fact.ScegliePortataFact;
import com.touriexpert.rest.reasoning.fact.TipoIngredienteFact;
import com.touriexpert.rest.reasoning.fact.VietataFact;
import com.touriexpert.ws.exception.ConnectionException;
import com.touriexpert.ws.exception.ReasoningException;

import client.ReasoningServiceClient;
import rdb.database.pojo.Categoria;
import rdb.database.pojo.Ingrediente;

public class MenuReasoning {
	private String projectName;
	private ReasoningServiceClient client;
	private static int LASTFACTID = 0;
	private String kb = "kb_touriexpert";
	private static MenuReasoning singleton;
	private GsonBuilder gsonbuilder;

	private MenuReasoning(GsonBuilder gsonBuilder) throws ConnectionException {
		try {
			this.gsonbuilder = gsonBuilder;
			URL url = new URL("http://127.0.0.1:8084/RPC3");
			this.client = new ReasoningServiceClient(url, "demoorganization", "demopassword");

			this.projectName = "proj" + 1;
			if(!this.client.existsProject(this.projectName))
				this.client.createProject(this.projectName);
		} catch(Exception e){
			throw new ConnectionException("Impossibile connettersi a Relay");
		}
	}

	public static MenuReasoning getInstance(GsonBuilder gsonBuilder) throws ConnectionException {
		if(singleton == null){
			MenuReasoning.singleton = new MenuReasoning(gsonBuilder);
		}
		return MenuReasoning.singleton;
	}

	public Fact[] sendArticolo(Integer idArticolo, Ingrediente ingredientePrincipale, 
			List<Ingrediente> ingredienti, Double costo, Set<Categoria> categorie) throws ReasoningException {
		Fact[] facts = null;
		try{
			String ingrediente = this.formatDbStringForRelay(ingredientePrincipale.getNome());
			
			IngredientePrincipaleFact ingPrinFact = new IngredientePrincipaleFact(LASTFACTID);
			ingPrinFact.setIdArticolo(idArticolo);
			ingPrinFact.setIngredientePrincipale(ingrediente);
			this.client.sendFactToProject(this.projectName, ingPrinFact.toString());
			LASTFACTID++;
			
			Set<Categoria> categorieIngrediente = ingredientePrincipale.getCategorie();
			if (categorieIngrediente.size() > 0) {
				for (Categoria c : categorieIngrediente) {
					String catIngrediente = this.formatDbStringForRelay(c.getNome());
					TipoIngredienteFact tipoIngredienteFact = new TipoIngredienteFact(LASTFACTID);
					tipoIngredienteFact.setIngrediente(ingrediente);
					tipoIngredienteFact.setTipo(catIngrediente);
					this.client.sendFactToProject(this.projectName, tipoIngredienteFact.toString());
					LASTFACTID++;
				}
			} else {
				throw new ReasoningException("Nessuna categoria per l'ingrediente principale!. "
						+ "È necessario avere una categoria per inferire il tipo di portata");
			}
			facts = this.gsonbuilder.create().fromJson(this.client.startInferenceJSON(this.projectName, this.kb), 
					Fact[].class);
			
			LASTFACTID = facts[facts.length - 1].getId() + 1;
			CostoPortataFact costoFact = new CostoPortataFact(LASTFACTID);
			costoFact.setIdArticolo(idArticolo);
			costoFact.setCosto(costo);
			facts = ArrayUtils.add(facts, costoFact);
			LASTFACTID++;
			VietataFact vietataFact = new VietataFact(LASTFACTID);
			vietataFact.setIdArticolo(idArticolo);
			vietataFact.setCertezza(0);
			facts = ArrayUtils.add(facts, vietataFact);
			LASTFACTID++;
			RankFact rankFact = new RankFact(LASTFACTID);
			rankFact.setIdArticolo(idArticolo);
			facts = ArrayUtils.add(facts, rankFact);
			LASTFACTID++;
			ContieneIngredienteFact contieneFact = null;
			for(Ingrediente i : ingredienti) {
				if (i.getNome().contains("principale_")){
					continue;
				}
				contieneFact = new ContieneIngredienteFact(LASTFACTID);
				String formattedNome = this.formatDbStringForRelay(i.getNome());
				contieneFact.setIngrediente(formattedNome);
				contieneFact.setIdArticolo(idArticolo);
				facts = ArrayUtils.add(facts, contieneFact);
				LASTFACTID++;
			}
			for (Categoria categoria : categorie) {
				CategoriaPortataFact factCategoria = new CategoriaPortataFact(LASTFACTID);
				factCategoria.setCategoria(categoria.getNome());
				factCategoria.setIdArticolo(idArticolo);
				facts = ArrayUtils.add(facts, factCategoria);
				LASTFACTID++;
			}
		} catch(Exception e){
			throw new ReasoningException(e);
		}
		return facts;
	}
	
	private String formatDbStringForRelay(String nonFromatted){
		String formattedName = new String(nonFromatted);
		formattedName = formattedName.replaceAll("'", "_").toLowerCase();
		return formattedName;
	}

	public Fact[] sendSelezionato(ReasArticolo articolo, List<Fact> factList) 
			throws ReasoningException {
		Fact[] facts = null;
		try{
//			for (Categoria categoria : articolo.getCategorie()) {
//				CategoriaPortataFact factCategoria = new CategoriaPortataFact(FACTID);
//
//				factCategoria.setCategoria(categoria.getNome());
//				factCategoria.setIdArticolo(articolo.getId());
//				System.out.println(factCategoria.toString());
//				this.client.sendFactToProject(this.projectName, factCategoria.toString());
//				FACTID++;
//			}
			ScegliePortataFact factSceglie = new ScegliePortataFact();
			LASTFACTID++;
			factSceglie.setId(LASTFACTID);
			factSceglie.setIdArticolo(articolo.getId());
			factSceglie.setIdUtente(0);
			factList.add(factSceglie);
			System.out.println(factSceglie.toString());
			this.client.sendFactToProject(this.projectName, factSceglie.toString());
			LASTFACTID++;
			facts =  this.gsonbuilder.create().fromJson(this.client.startInferenceJSON(this.projectName, this.kb), 
					Fact[].class);
		} catch(Exception e){
			throw new ReasoningException(e);
		}
		return facts;
	}

	public void sendFact(Fact fact) throws Exception {
		this.client.sendFactToProject(this.projectName, fact.toString());
	}
	
	public Fact[] reason() throws Exception {
		return this.gsonbuilder.create().fromJson(this.client.startInferenceJSON(this.projectName, this.kb), 
				Fact[].class);
	}

	public void addFactsTo(ReasArticolo reasArticolo, Fact[] facts) {
		reasArticolo.setFacts(facts);
	}
	
	public void updateLastFactId(int factid) {
		LASTFACTID = factid;
	}
}