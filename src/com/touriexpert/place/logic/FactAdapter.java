package com.touriexpert.place.logic;

import java.lang.reflect.Type;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import com.touriexpert.rest.reasoning.fact.Fact;
import com.touriexpert.rest.reasoning.fact.FactParser;
import com.touriexpert.ws.exception.FactParserException;

public class FactAdapter implements JsonSerializer<Fact>, JsonDeserializer<Fact> {

	@Override
	public Fact deserialize(JsonElement json, Type type, JsonDeserializationContext context) throws JsonParseException {
		FactParser parser = new FactParser();
		JsonObject jobject = json.getAsJsonObject();
		Fact fact = null;
		try {
			fact = parser.getFact(jobject.get("body").getAsString());
		} catch (FactParserException e) {
			throw new JsonParseException(e.getMessage());
		}
		fact.setId(jobject.get("id").getAsInt());
		fact.setCertezza(jobject.get("certezza").getAsDouble());
		
		return fact;
	}

	@Override
	public JsonElement serialize(Fact fact, Type type, JsonSerializationContext context) {
		JsonObject obj = new JsonObject();
		obj.addProperty("id", fact.getId());
		obj.addProperty("body", fact.getBody());
		obj.addProperty("certezza", fact.getCertezza());
		return obj;
	}
	
}
