package com.touriexpert.place.logic;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import com.touriexpert.rest.models.ReasArticolo;
import com.touriexpert.rest.reasoning.fact.Fact;

import rdb.database.pojo.Articolo;
import rdb.database.pojo.Categoria;
import rdb.database.pojo.Ingrediente;
import rdb.database.pojo.Ricetta;

public class ArticoloAdapter implements JsonDeserializer<ReasArticolo>, JsonSerializer<ReasArticolo> {

	private Map<String, Object> setIngredientiInfo(Ricetta r){
		Map<String, Object> ingInfo = new HashMap<String, Object>();
		ArrayList<String> arrayCategorie = new ArrayList<String>();
		Ingrediente ing = r.getId().getIngrediente();
		ingInfo.put("id", ing.getIdprodotto());
		for(Categoria c : ing.getCategorie()){
			arrayCategorie.add(c.getNome());
		}
		ingInfo.put("categorie", arrayCategorie);
		return ingInfo;
	}
	
	@Override
	public JsonElement serialize(ReasArticolo articolo, Type typeOfSrc, JsonSerializationContext context) {
		JsonObject obj = new JsonObject();
		HashMap<String, Object> mapIngredienti = new HashMap<String, Object>();
        obj.addProperty("id", articolo.getId());
        obj.addProperty("nome", articolo.getNome());
        obj.addProperty("descrizione", articolo.getDescrizione());
        obj.addProperty("prezzo", articolo.getPrezzo());
        for(Ricetta r : articolo.getRicette()){
			Ingrediente i = r.getId().getIngrediente();
			if(i.getNome().contains("principale_")){
				for(Categoria c : i.getCategorie()){
					mapIngredienti.put(i.getNome(), c.getNome());
				}
			} else {
				mapIngredienti.put(i.getNome(), this.setIngredientiInfo(r));
			}
		}
        JsonElement facts = context.serialize(articolo.getFacts());
        obj.add("facts", facts);
        JsonElement schedaMercadini = context.serialize(articolo.getSchedaMercadini());
        obj.add("mercadini", schedaMercadini);
        return obj;
	}

	@Override
	public ReasArticolo deserialize(JsonElement json, Type type, JsonDeserializationContext context)
			throws JsonParseException {
		Articolo art = new Articolo();
		JsonObject obj = json.getAsJsonObject();
		art.setIdarticolo(obj.get("id").getAsInt());
		art.setNome(obj.get("nome").getAsString());
		art.setDescrizione(obj.get("descrizione").getAsString());
		art.setPrezzo(obj.get("prezzo").getAsDouble());
		Fact[] facts = context.deserialize(obj.get("facts"), Fact[].class);
		ReasArticolo reasArticolo = new ReasArticolo(art);
		reasArticolo.setSchedaMercadini(context.deserialize(obj.get("mercadini"), HashMap.class));
		reasArticolo.setFacts(facts);
		reasArticolo.setArticolo(art);
		return reasArticolo;
	}
}
