package com.touriexpert.place.logic;

import java.util.Set;

import org.apache.commons.lang3.StringUtils;

import rdb.database.pojo.Articolo;
import rdb.database.pojo.Categoria;
import rdb.database.pojo.Ingrediente;
import rdb.database.pojo.Ricetta;

public class MenuParser {
	
	public Ingrediente getIngredientePrincipale(Articolo articolo) {
		Set<Ricetta> ricette = articolo.getRicette();
		String idPrincipale = this.getIdPrincipale(articolo);
		Ingrediente ingPrincipale = null;
		for(Ricetta r : ricette){
			Ingrediente ing = r.getId().getIngrediente();
			String idIngrediente = ing.getIdprodotto().toString();
			if(idIngrediente.equals(idPrincipale)){
				ingPrincipale = ing;
				break;
			}
		}
		return ingPrincipale;
	}
	
	private String getIdPrincipale(Articolo articolo){
		Set<Ricetta> ricette = articolo.getRicette();
		String idPrincipale = "";
		for(Ricetta r : ricette){
			Ingrediente ing = r.getId().getIngrediente();
			Set<Categoria> setCat = ing.getCategorie();
			if (setCat.size() == 1) {
				Categoria c = (Categoria) setCat.toArray()[0];
				String catNome = c.getNome();
				if(this.isNumeric(catNome)){
					idPrincipale = catNome;
					break;
				}
			}
		}
		return idPrincipale;
	}
	
	private boolean isNumeric(String str) {
		return StringUtils.isNumeric(str);
	}

}
