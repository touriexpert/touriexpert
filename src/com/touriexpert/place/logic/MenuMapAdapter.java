package com.touriexpert.place.logic;

import java.lang.reflect.Type;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import com.touriexpert.rest.models.ReasArticolo;

public class MenuMapAdapter implements JsonDeserializer<MenuMap>, JsonSerializer<MenuMap> {
	
	@Override
	public JsonElement serialize(MenuMap menuMap, Type typeOfSrc, JsonSerializationContext context) {
		JsonObject obj = new JsonObject();
		obj.addProperty("id", menuMap.getIdMenu());
		this.addCategorie(obj, menuMap);
		this.addArticoli(obj, menuMap, context);
		obj.addProperty("lastFactID", menuMap.getLastFactID());
		return obj;
	}
	
	private void addCategorie(JsonObject obj, MenuMap menuMap){
		JsonArray categorie = new JsonArray();
		for(String categoria: menuMap.getCategorie()){
			categorie.add(categoria);
		}
		obj.add("categorie", categorie);
	}
	
	private void addArticoli(JsonObject obj, MenuMap menuMap, JsonSerializationContext context){
		JsonObject articoliCategoria = new JsonObject();
		JsonArray articoliArray = null;
		for(String categoria : menuMap.getCategorie()){
			articoliArray = new JsonArray();
			List<ReasArticolo> articoli = menuMap.getArticoli(categoria);
			for (ReasArticolo articolo : articoli) {
				JsonElement serializedArticolo = context.serialize(articolo);
				articoliArray.add(serializedArticolo);
			}
			articoliCategoria.add(categoria, articoliArray);
		}
		obj.add("articoli", articoliCategoria);
	}

	@Override
	public MenuMap deserialize(JsonElement json, Type type, JsonDeserializationContext context) throws JsonParseException {
		MenuMap menu = new MenuMap();
		JsonObject jobject = json.getAsJsonObject();
		
		menu.setIdMenu(jobject.get("id").getAsInt());
		
		JsonArray categorieJson = jobject.get("categorie").getAsJsonArray();
		this.setCategorie(menu, categorieJson);
		
		JsonObject articoli = jobject.get("articoli").getAsJsonObject();
		this.setArticoli(menu, articoli, context);
		
		int lastFactId = jobject.get("lastFactID").getAsInt();
		menu.setLastFactID(lastFactId);
		
		return menu;
	}
	
	private void setCategorie(MenuMap menu, JsonArray categorieJson){
		Set<String> categorie = new HashSet<String>();
		Iterator<JsonElement> i =  categorieJson.iterator();
		while(i.hasNext()){
			JsonElement categoriaJson = (JsonElement)i.next();
			String categoriastr = categoriaJson.getAsString();
			categorie.add(categoriastr);
		}
		menu.setCategorie(categorie);
	}
	
	private void setArticoli(MenuMap menu, JsonObject articoli, JsonDeserializationContext context) {
		for(String categoria : menu.getCategorie()){
			JsonArray articoliCategoria = articoli.get(categoria).getAsJsonArray();
			Iterator<JsonElement> i =  articoliCategoria.iterator();
			while(i.hasNext()){
				JsonElement articolo = (JsonElement)i.next();
				ReasArticolo reasArticolo = context.deserialize(articolo, ReasArticolo.class);
				menu.addArticolo(categoria, reasArticolo);
			}
		}
	}
 
}