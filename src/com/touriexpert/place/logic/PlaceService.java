package com.touriexpert.place.logic;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import org.apache.commons.lang3.ArrayUtils;

import com.google.gson.GsonBuilder;
import com.touriexpert.rest.models.ExtOrdinazioni;
import com.touriexpert.rest.models.ExtPrenotazione;
import com.touriexpert.rest.models.ReasArticolo;
import com.touriexpert.rest.models.SelMenuItem;
import com.touriexpert.rest.reasoning.MenuReasoning;
import com.touriexpert.rest.reasoning.fact.Fact;
import com.touriexpert.rest.reasoning.fact.UpdateRankFact;
import com.touriexpert.ws.exception.ConnectionException;

import placeapi.PlaceAPI;
import rdb.database.pojo.Articolo;
import rdb.database.pojo.Attivita;
import rdb.database.pojo.Cliente;
import rdb.database.pojo.Comanda;
import rdb.database.pojo.Ordinazione;
import rdb.database.pojo.Prenotazione;

public final class PlaceService {
	private static PlaceService singleton;
	private PlaceAPI placeAPI;
	private GsonBuilder gsonBuilder;
	private MenuReasoning menuReasoning;

	private PlaceService(GsonBuilder gsonBuilder, PlaceAPI placeAPI, MenuReasoning placeReasoning) throws ConnectionException {
		this.placeAPI = placeAPI;
		this.gsonBuilder = gsonBuilder;
		this.menuReasoning = placeReasoning;
	}

	public static PlaceService getInstance(GsonBuilder gsonBuilder, PlaceAPI placeAPI, MenuReasoning placeReasoning) throws ConnectionException {
		if(singleton == null){
			PlaceService.singleton = new PlaceService(gsonBuilder, placeAPI, placeReasoning);
		}
		return PlaceService.singleton;
	}

	public Map<String, Object> getInfo(Integer placeID){ 
		Attivita attivita = this.placeAPI.findAttivitaByID(placeID);
		Map<String, Object> placeInfos = new HashMap<String, Object>();
		placeInfos.put("id", attivita.getIdattivita());
		placeInfos.put("nome", attivita.getRagionesociale());
		placeInfos.put("indirizzo", attivita.getIndirizzo());
		placeInfos.put("telefono", attivita.getTel());
		placeInfos.put("website", "http://www.ristorantelapignatabari.com/");
		placeInfos.put("email", attivita.getEmail());
		String[] giorni = {"Lunedì", "Martedì", "Mercoledì", 
				"Giovedì", "Venerdì", "Sabato", "Domenica"};
		Map<String, Object> openings = new TreeMap<String, Object>(new Comparator<String>() {
			public int compare(String o1, String o2) {
				int index1 = ArrayUtils.indexOf(giorni, o1);
				int index2 = ArrayUtils.indexOf(giorni, o2);
				int res = 0;
				if(index1 < index2){
					res = -1;
				} else if(index1 > index2){
					res = 1;
				} else if(index1 == index2){
					res = 0;
				}
				return res;
			}
		});
		for(int i = 0; i < 7; i++){
			String[] test = new String[2];
			test[0] = "10:00";
			test[1] = "23:00";
			openings.put(giorni[i], test);
		}
		placeInfos.put("orari", openings);
		return placeInfos;
	}

	public Integer getTables(Integer placeID){
		return this.placeAPI.getTables(1);
	}

	public String getMenu(Integer placeID, Time oraPrenotazione, 
			String sessionCode) throws ConnectionException {
		InputStream inputStream = PlaceService.class.getResourceAsStream("menu.json");
		BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
		String aDataRow = "";
        String aBuffer = "";
        try {
			while ((aDataRow = reader.readLine()) != null) {
			    aBuffer += aDataRow ;
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
        MenuMap menu = this.gsonBuilder.create().fromJson(aBuffer, MenuMap.class);
//		Menu menu = this.placeAPI.getMenu(placeID, oraPrenotazione, sessionCode);
//		Set<String> categorieMenu = this.placeAPI.getCategorieMenu(menu.getIdmenu());
//		MenuMap menuMap = new MenuMap();
//		MenuParser menuParser = new MenuParser();
//		
//		menuMap.setIdMenu(menu.getIdmenu());
//		menuMap.setCategorie(categorieMenu);
//		
//		for(Articolo art : menu.getArticoli()){
//			ReasArticolo reasArticolo = new ReasArticolo(art);
//			Ingrediente ingPrincipale = menuParser.getIngredientePrincipale(art);
//			Set<Ricetta> ricette = art.getRicette();
//			List<Ingrediente> ingredienti = new ArrayList<Ingrediente>();
//			for (Ricetta r : ricette) {
//				Ingrediente i = r.getId().getIngrediente();
//				ingredienti.add(i);
//			}
//			try {
//				Fact[] facts = this.menuReasoning.sendArticolo(art.getIdarticolo(), ingPrincipale, 
//						ingredienti, art.getPrezzo(), art.getCategorie());
//				this.menuReasoning.addFactsTo(reasArticolo, facts);
//				Fact lastFact = facts[facts.length - 1];
//				menuMap.setLastFactID(lastFact.getId());
//			} catch (Exception e) {
//				throw new ConnectionException(e);
//			}
//			for(Categoria cat : art.getCategorie()){
//				menuMap.addArticolo(cat.getNome(), reasArticolo);
//			}
//		}
      return this.gsonBuilder.create().toJson(menu);
	}
	
	public String updateMenu(MenuMap menu, ReasArticolo[] selectionList, String sessionCode) throws Exception {
		Set<String> categorieMenu = menu.getCategorie();
		List<Fact> factList = new ArrayList<Fact>();
		for(String categoria : categorieMenu) {
			factList.addAll(this.getArticoliFacts(menu.getArticoli(categoria)));
		}
		for(Fact fact : factList){
			System.out.println(fact);
			this.menuReasoning.sendFact(fact);
		}
		this.menuReasoning.updateLastFactId(menu.getLastFactID());
		for(ReasArticolo art : selectionList) {
			Articolo articoloDB = this.placeAPI.getArticoloByID(art.getId());
			art.setArticolo(articoloDB);
			Fact[] facts = this.menuReasoning.sendSelezionato(art, factList);
			System.out.println(Arrays.toString(facts));
			factList.addAll(Arrays.asList(facts));
			for (Fact fact : factList) {
				if (fact instanceof UpdateRankFact) {
					UpdateRankFact updateRankFact = (UpdateRankFact) fact;
					ReasArticolo updateArticolo = menu.getArticolo(updateRankFact.getIdArticolo());
					updateArticolo.updateRank(updateRankFact.getValue());
				}
			}
		}
		return this.gsonBuilder.create().toJson(menu);
	}
	
	private List<Fact> getArticoliFacts(List<ReasArticolo> articoli) throws Exception 
	{
		List<Fact> listFacts = new ArrayList<Fact>();
		for(ReasArticolo art : articoli){
			Fact[] artFacts = art.getFacts();
			for(Fact fact : artFacts){
				listFacts.add(fact);
			}
		}
		return listFacts;
	}
	
	private Comanda setComanda(Attivita attivita, ExtOrdinazioni[] extOrdinazioni, int coperti){
		Set<Ordinazione> ordinazioni = new HashSet<Ordinazione>();
		Date data = new Date();
		Timestamp dataCreazione = new Timestamp(data.getTime());
		Comanda comanda = new Comanda();
		for(ExtOrdinazioni menu : extOrdinazioni){
			for(SelMenuItem item : menu.getMenu()){
				Ordinazione ord = new Ordinazione();
				Articolo art = this.placeAPI.getArticoloByID(item.getId());
				art.setAttivita(attivita);
				ord.setArticolo(art);
				ord.setQuantita(1);
				ord.setPrezzo(art.getPrezzo());
				ord.setPagato(false);
				ord.setComanda(comanda);
				ordinazioni.add(ord);
			}
		}
		int copertiComanda = ordinazioni.size();
		comanda.setCoperti(copertiComanda);
		comanda.setDatacreazione(dataCreazione);
		comanda.setPagata(false);
		comanda.setAttiva(true);
		comanda.setOrdinazioni(ordinazioni);
		return comanda;
	}
	
	private void setCostoComanda(Comanda comanda){
		Double costo = 0.0;
		for(Ordinazione ord : comanda.getOrdinazioni()){
			costo += ord.getPrezzo();
		}
		comanda.setImporto(costo);
	}
	
	private Prenotazione setPrenotazione(ExtPrenotazione extPrenotazione){
		Prenotazione prenotazione = new Prenotazione();
		int coperti = extPrenotazione.getPeopleMenu() + extPrenotazione.getPeopleNoMenu(); 
		prenotazione.setCoperti(coperti);
		prenotazione.setChekin(extPrenotazione.getRealTime());
		prenotazione.setDelay(0.0);
		Date dataCreazione = new Date();
		Timestamp timeCreazione = new Timestamp(dataCreazione.getTime());
		prenotazione.setDatacreazione(timeCreazione);
		return prenotazione;
	}
	
	public Double getCostoReservation(Integer placeID){
		return 10.0;
	}

	public Map<String, Object> saveReservation(Integer placeID, ExtOrdinazioni[] extOrdinazioni, 
			ExtPrenotazione extPrenotazione, String sessionCode){
		Map<String, Object> res = new HashMap<String, Object>();
		Attivita attivita = this.placeAPI.findAttivitaByID(placeID);
		Cliente cliente = this.placeAPI.getCliente(sessionCode);
		Prenotazione prenotazione = this.setPrenotazione(extPrenotazione);
		prenotazione.setCliente(cliente);
		prenotazione.setIdcliente(cliente.getIdcliente());
		if(extOrdinazioni.length > 0){
			prenotazione.setAttivita(attivita);
			Comanda comanda = this.setComanda(attivita, extOrdinazioni, 
					extPrenotazione.getPeopleMenu());
			this.setCostoComanda(comanda);
			comanda.setPrenotazione(prenotazione);
			comanda.setCliente(cliente);
			comanda.setIdcliente(cliente.getIdcliente());
			comanda.setAttivita(attivita);
			comanda.setIdattivita(attivita.getIdattivita());
			prenotazione.setComanda(comanda);
			prenotazione.setCosto(10.0);
			prenotazione = this.placeAPI.savePrenotazione(prenotazione, sessionCode);
			res.put("idPrenotazione", prenotazione.getIdprenotazione());
			res.put("idComanda", comanda.getIdcomanda());
			res.put("costoPrenotazione", this.getCostoReservation(placeID));
			res.put("costoComanda", comanda.getImporto());
		} else {
			prenotazione = this.placeAPI.savePrenotazione(prenotazione, sessionCode);
		}
		return res;
	}

}
