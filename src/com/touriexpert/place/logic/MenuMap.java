package com.touriexpert.place.logic;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.touriexpert.rest.models.ReasArticolo;

public class MenuMap {
	private int idMenu;
	private Set<String> categorie;
	private Map<String, ArrayList<ReasArticolo>> articoli;
	private Map<Integer, String> idCategoria;
	private List<ReasArticolo> selezionati;
	private int lastFactID;

	MenuMap(){
		this.articoli = new HashMap<String, ArrayList<ReasArticolo>>();
		this.idCategoria = new HashMap<Integer, String>();
	}
	
	public void addArticolo(String categoria, ReasArticolo reasArticolo){
		if (this.articoli.containsKey(categoria) == false) {
			ArrayList<ReasArticolo> listArt = new ArrayList<ReasArticolo>();
			listArt.add(reasArticolo);
			this.articoli.put(categoria, listArt);
		} else {
			this.articoli.get(categoria).add(reasArticolo);
		}
		this.idCategoria.put(reasArticolo.getId(), categoria);
	}
	
	public ReasArticolo getArticolo(Integer idArticolo) {
		ReasArticolo articolo = null;
		String categoria = this.idCategoria.get(idArticolo);
		List<ReasArticolo> listArticoli = this.articoli.get(categoria);
		for (ReasArticolo art : listArticoli) {
			if (art.getId() == idArticolo) {
				articolo = art;
				break;
			}
		}
		return articolo;
	}

	public List<ReasArticolo> getArticoli(String categoria) {
		return articoli.get(categoria);
	}

	public int getIdMenu() {
		return idMenu;
	}

	public void setIdMenu(int idMenu) {
		this.idMenu = idMenu;
	}

	public Set<String> getCategorie() {
		return categorie;
	}
	
	public void setCategorie(Set<String> categorieMenu){
		this.categorie = categorieMenu;
	}

	public List<ReasArticolo> getSelezionati() {
		return selezionati;
	}

	public void setSelezionati(List<ReasArticolo> selezionati) {
		this.selezionati = selezionati;
	}
	
	public int getLastFactID() {
		return lastFactID;
	}

	public void setLastFactID(int lastFactID) {
		this.lastFactID = lastFactID;
	}
}