package com.touriexpert.tests;

import java.util.ArrayList;
import java.util.Map;

import com.touriexpert.rest.models.ExtOrdinazioni;
import com.touriexpert.rest.models.ExtPrenotazione;
import com.touriexpert.rest.models.SelMenuItem;

public class ReservationTest extends TouriExpertTest {

	public String testSaveReservation(){
		ExtPrenotazione extPrenotazione = new ExtPrenotazione();
		extPrenotazione.setDate("2016-06-21");
		extPrenotazione.setTime("21:30");
		extPrenotazione.setPeopleMenu(1);
		extPrenotazione.setPeopleNoMenu(0);
		extPrenotazione.setRealDate();
		extPrenotazione.setRealTime();
		ExtOrdinazioni[] extOrdinazioni = new ExtOrdinazioni[1];
		SelMenuItem item = new SelMenuItem();
		item.setId(1);
		item.setNome("Prova");
		ExtOrdinazioni ordinazione = new ExtOrdinazioni();
		ArrayList<SelMenuItem> listOrdinazioni = new ArrayList<SelMenuItem>();
		listOrdinazioni.add(item);
		ordinazione.setMenu(listOrdinazioni);
		extOrdinazioni[0] = ordinazione;
		Map<String, Object> savedResData = this.placeService.saveReservation(1, 
				extOrdinazioni, extPrenotazione, null);
		return this.gson.toJson(savedResData);
	}

}