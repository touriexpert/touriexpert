package touriexpert.ws;

import java.net.URL;
import java.sql.Time;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.touriexpert.place.logic.ArticoloAdapter;
import com.touriexpert.place.logic.FactAdapter;
import com.touriexpert.place.logic.MenuMap;
import com.touriexpert.place.logic.MenuMapAdapter;
import com.touriexpert.place.logic.PlaceService;
import com.touriexpert.rest.models.ExtOrdinazioni;
import com.touriexpert.rest.models.ExtPrenotazione;
import com.touriexpert.rest.models.ReasArticolo;
import com.touriexpert.rest.models.SelMenuItem;
import com.touriexpert.rest.reasoning.MenuReasoning;
import com.touriexpert.rest.reasoning.fact.Fact;
import com.touriexpert.ws.exception.ConnectionException;
import com.touriexpert.ws.exception.ParameterException;

import client.ReasoningServiceClient;
import placeapi.APIFactory;
import placeapi.PlaceAPI;
import rdb.database.pojo.Articolo;

@SuppressWarnings("unused")
public class TouriExpertWS {
	private PlaceService placeService;
	private GsonBuilder gsonBuilder;
	private Gson gson;

	public TouriExpertWS() {
		this.gson = new Gson();
		this.gsonBuilder = new GsonBuilder();
		this.gsonBuilder.registerTypeAdapter(MenuMap.class, new MenuMapAdapter());
		this.gsonBuilder.registerTypeAdapter(ReasArticolo.class, new ArticoloAdapter());
		this.gsonBuilder.registerTypeAdapter(Fact.class, new FactAdapter());
		PlaceAPI placeAPI = APIFactory.getInstance().getPlaceAPI();
		try {
			MenuReasoning menuReasoning = MenuReasoning.getInstance(this.gsonBuilder);
			this.placeService = PlaceService.getInstance(this.gsonBuilder, placeAPI, menuReasoning);
		} catch (ConnectionException e) {
			e.printStackTrace();
		}
	}

	public String getPlaceInfo(String placeID) throws ParameterException{
		int idAttivita;
		try{
			idAttivita = Integer.parseInt(placeID);
		} catch(NumberFormatException e){
			throw new ParameterException(e);
		}
		Map<String, Object> placeInfo = this.placeService.getInfo(idAttivita);
		return this.gson.toJson(placeInfo);
	}

	public Integer getPlaceTables(String placeID) throws ParameterException{
		//		try{
		//			int id = Integer.parseInt(placeID);
		//		} catch(NumberFormatException e){
		//			throw new ParameterException(e);
		//		}
		return this.placeService.getTables(1);
	}

	public String sendReservation(String placeID, String reservationData, 
			String menuID, String ordinazioni, String sessionCode) throws ParameterException {
		int idAttivita, idMenu;

		try{
			idAttivita = Integer.parseInt(placeID);
			idMenu = 1;
		} catch(NumberFormatException e){
			throw new ParameterException(e);
		}

		ExtPrenotazione extPrenotazione = this.gson.fromJson(reservationData, 
				ExtPrenotazione.class);
		String date = extPrenotazione.getDate();
		String time = extPrenotazione.getTime();
		extPrenotazione.setRealDate();
		extPrenotazione.setRealTime();
		ExtOrdinazioni[] extOrdinazioni = this.gson.fromJson(ordinazioni, 
				ExtOrdinazioni[].class);
		Map<String, Object> savedResData = this.placeService.saveReservation(idAttivita, 
				extOrdinazioni, extPrenotazione, sessionCode);
		return this.gson.toJson(savedResData);
	}

	public boolean addMenuItem(String placeID, String item, String sessionCode) 
			throws ParameterException{
		try{
			int id = Integer.parseInt(placeID);
		} catch(NumberFormatException e){
			throw new ParameterException(e);
		}
		return false;
	}

	public String getMenu(String placeID, String reservationDate, 
			String reservationTime, String sessionCode) throws ParameterException, ConnectionException {
		int idAttivita;
		try{
			idAttivita = Integer.parseInt(placeID);
		} catch(NumberFormatException e){
			throw new ParameterException(e);
		}
		Date oraPrenotazione = new Date();
		Time timePrenotazione = null;
		try{
			SimpleDateFormat oraFormat = new SimpleDateFormat("HH:mm:ss");
			String strTimePrenotazione = "21:18:00";
			oraPrenotazione = oraFormat.parse(strTimePrenotazione);
			timePrenotazione = new Time(oraPrenotazione.getTime());
		} catch(ParseException e){
			throw new ParameterException(e);
		}
		String placeMenu = this.placeService.getMenu(idAttivita, timePrenotazione, sessionCode);
		return placeMenu;
	}

	public String selected(String menu, String selectionList, String sessionCode) 
			throws Exception {
		MenuMap menuMap = null;
		try {
			menuMap = this.gsonBuilder.create().fromJson(menu, MenuMap.class);
		} catch(Exception e) {
			throw new Exception(e);
		}
		ReasArticolo[] listaSelezionati = null;
		try {
			listaSelezionati = this.gsonBuilder.create().fromJson(selectionList, ReasArticolo[].class);
		} catch(Exception e){
			throw new Exception(e);
		}
		return this.placeService.updateMenu(menuMap, listaSelezionati, sessionCode);
	}

	public boolean savePaymentProof(Integer idPrenotazione, Integer idComanda, String paymentCode, 
			String sessionCode) {
		return true;
	}
}
