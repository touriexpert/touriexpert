package touriexpert.ws;

import org.apache.axis.AxisFault;
import org.apache.axis.MessageContext;
import org.apache.axis.handlers.BasicHandler;
import org.apache.log4j.Logger;

public class SOAPLogHandler extends BasicHandler {

	private static Logger LOG= Logger.getLogger(SOAPLogHandler.class);
	private static final long serialVersionUID = 1L;

	@Override
	public void invoke(MessageContext msgContext) throws AxisFault {
		if(msgContext.getResponseMessage() != null && msgContext.getResponseMessage().getSOAPPart() != null) {
			LOG.info(" Response = " + msgContext.getResponseMessage().getSOAPPartAsString());
		} else {
			if(msgContext.getRequestMessage() != null && msgContext.getRequestMessage().getSOAPPartAsString() != null) {
				LOG.info(" Request = " + msgContext.getRequestMessage().getSOAPPartAsString());
			}    
		}
	} 
}